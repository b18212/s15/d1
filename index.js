// alert("Hello Agian!");

// Syntax, alert("message");

/*
	Syntax, Statements and Comments

	Statements -  programming instructions that we tell out computer / machine to perform
	>> JS Statements - usually ends with semi-colon (;)
		>> semi-colon helps to locate where statements end
	
	Syntax - set of rules describing how statements must be constructed

	Comments - are parts of the code that is ignored
		>> meant to describe the written code
		>> two-types comments
			>> single line comments - double slash
			>> multi-line comments - slash and asterisks

		>> 
*/

// console.log = prints in the console
console.log("Hello World");

// Variables
	
	// Declaring variables

	/* 
		Syntax: let/ const variableName;
	*/
	let myVariable;
	console.log(myVariable); // result: undefined

	let hello;
	console.log(hello); // result: not defined if a variable is not declared first
	// let hello; cannot access before initialization

		// Guides:
// Declaring and initializing variables
	/*
		Syntax: let/ const variableName = value;
	*/
	
let productName = "desktop computer";
console.log(productName);

let price = 18999;
console.log(price);

// const variables - information / values that shouldn't be changed

const interest = 3.539;
console.log(interest);

// Reasigning variables
/*
	Syntax: variableName = value;
*/

/* Camel casing - lowercase letter then uppercase letter */
productName = "laptop";
console.log(productName);

let friend = "Min";
friend = "Jane";
console.log(friend); //result: Jane

// let friend = "Kate";
// console.log(friend); //result: error: identifier friend has already been declared

// interest = 3.614;
// console.log(interest); // result: assignment to constant variable

let role = "Supervisor";
let name = "Edward";

/*
	Mini-Activity:
	1. Print out the value of role and name
	2. re-assigned the value of role to director
	3. send a screenshot of your console with the output
*/



console.log(role);
console.log(name);
role = "Director";
console.log(role);


// Reassigning variables vs. Initializing variables
// Declare a variable
// initialization - done after a variable has been declared
let supplier;
supplier = "Jane Smith Tradings";
console.log(supplier); //result: Jane Smith Tradings

supplier = "Zuitt Store";
console.log(supplier); //result: Zuit Store

/* const variable should be
const pi;
pi = 3.1416;
console.log(pi); 
*/

// var vs. let/ const

// var - was used from 1997 to 2015
// let / const -  ES6 updates (2015)


// Hoisting of var - Javascript default behavior of moving declaration to the top
a = 5;
console.log(a); result: 5
var a;

// let / const local and global scope
// Scope - essentially means where these variables are available for use


// global variable
let outerVariables = "Hello";

{
	//local variable
	// let / const are block scoped
	// block is a chunk of code surrounds by {}. A block loves in curly braces
	let innerVariable = "Hello Again";
};

// console.log(outerVariables); // result: hello
// console.log(innerVariable); // result: error

// Multiple Variable Declarations
// let productCode = "DC017", productBrand = "Dell";
let productCode = "DC017";
const productBrand = "Dell";
console.log(productCode, productBrand);


// const let = "hello";
// console.log(let): //result: error


// Data Types

// Strings
// Series of alphanumeric characters that create a word, phrase, a sentence or anything related to text


//strings are wrapped around quotation marks ('') or ("")
let country = "Philippines";
let city = 'Caloocan City';
let numberString = "123456";

// Concatinationg Strings
let fullAddress = city + ','  + ' ' + country;
console.log(fullAddress);


let myName = "Hannah";
let greeting = 'Hi I am';
console.log(greeting + ' ' + myName);


// escape character
let mailAddress = 'Metro Manila\n\nPhilippines';
console.log(mailAddress); //result line break between MM and PH

let message = "John's father is coming home today";
console.log(message); //print out the message

message = 'John\'s father is coming home tomorrow';
console.log(message)

let pokemon = "pikachu";
console.log(pokemon); //result: pikachu

// Numbers
// Integers/ Whole Numbers
let count = 64;
console.log(count); //result: 64

// Decimal Numbers / Fractions
let grade = 98.7;
console.log(grade); //result: 98.7

// Exponential Notations
let planetDistance = 2e10;
console.log(planetDistance); 


// Concatenate text and numbers
console.log('Jino\'s grade last quarter is' + ' ' + grade);

// consider where will you use it
let numberString1 = "09365645186";
let number = 4578;

// Boolean
// this will be usefuil in future discussion considering logic

let isSingle = true;
let inGoodConduct =  false;
console.log("isSingle" + ' ' + isSingle);
console.log("inGoodConduct" + ' ' + inGoodConduct);

// additional info:
	// 1 equivalent to true
	// 0 equivalent to false


// Arrays
// special kind of data type

/* 
	let / const arrayName = [elementA, elementB ...]
*/

let anime = ["Naruto", "Bleach", "Attack On Titan", "Demon Slayer", "Spy x Family"];
console.log(anime);

let quarterlyGrades = [95, 96.3, 87, 90];
console.log(quarterlyGrades);


// it works but doesn't make sense in the context programming and not recommended
let random = ["JK", 24, true];
console.log(random);

// Objects
// Special kind of data type

/*
	Syntax:
	let/ const objectName = {
		propertyA: valueA,
		propertyB: valueB,
	}

	Key-value pairs
*/

let person = {
	fullName: "Midoriya Izuku",
	age: 15,
	isStudent: true,
	contactNo: ["091234567899", "8123 4567"],
	address: {
		houseNumber: "568",
		city: "Tokyo"
	}
};

console.log(person);

// Null
let spouse = null;
console.log(spouse);

let emptyString = ""
console.log(emptyString);

let myNumber = 0;
console.log(myNumber);